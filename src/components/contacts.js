
const CONTACTS = [
    {
        firstName: 'al',
        lastName: 'pacino',
        group:'friends',
        phone: '10722437799'
    },
    {
        firstName: 'alain',
        lastName: 'delon',
        group:'friends',
        phone: '20722437799'
    },
    {
        firstName: 'antony',
        lastName: 'Hopkins',
        group:'work',
        phone: '30722437799'
    },
    {
        firstName: 'brad',
        lastName: 'pitt',
        group:'friends',
        phone: '40722437799'
    },
    {
        firstName: 'joaquin',
        lastName: 'pheonix',
        group:'work',
        phone: '50722437799'
    },
    {
        firstName: 'matt',
        lastName: 'damon',
        group:'friends',
        phone: '110722437799'
    },
    {
        firstName: 'michael',
        lastName: 'caine',
        group:'others',
        phone: '110722432227799'
    },
    {
        firstName: 'morgan',
        lastName: 'freeman',
        group:'work',
        phone: '110722432227799'
    },
    {
        firstName: 'tommy',
        lastName: 'lee jones',
        group:'family',
        phone: '110722432227799'
    }
]

export const groupOptions = [
    { value: 'work', text: 'Work' },
    { value: 'friends', text: 'Friends' },
    { value: 'family', text: 'Family' },
    { value: 'others', text: 'Others' },
  ]

export default CONTACTS