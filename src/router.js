import Vue from 'vue';
import VueRouter from 'vue-router';
import LoginPage from '../src/components/LoginPage.vue';
import MainApp from '../src/components/MainApp.vue';


Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        redirect: '/login'
    },
    {
        path: '/login',
        name: 'LoginPage',
        component: LoginPage
    },
    {
        path: '/mainApp',
        name: 'MainApp',
        component: MainApp
    },
];

const router = new VueRouter({
    mode: 'history',
    base: '/',
    routes,
});

export default router;
