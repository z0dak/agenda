// add new contact
export const addContact = (state, contact) => {
    state.contacts.push(contact);
}
// delete existing contact
export const deleteContact = (state, index) => {
    state.contacts.splice(index, 1);
}
// update existing contact
export const updateContact = (state, data) => {
    state.contacts.splice(data.contactIndex, 1, data.contact);
}